<?php

namespace Ervin11\HoneypotBundle\Types;

use Ervin11\HoneypotBundle\Event\HoneypotSpamDetectedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


class HoneypotType extends AbstractType
{
    public const CODE = 'SPAM';

    private $requestStack;
    private $logger;
    private $eventDispatcher;

    public function __construct
    (
        RequestStack             $requestStack,
        LoggerInterface          $mainLogger,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->requestStack = $requestStack;
        $this->logger = $mainLogger;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new Assert\NotNull(),
                        new Assert\Email(null, "The email '{{ value }}' is not a valid email.", "strict")
                    ]
                ])
            ->add('fakeEmail', EmailType::class,
                [
                    'label_attr' => [
                        'style' => 'font-size: 0;'
                    ],
                    'attr' => [
                        'style' => 'height: 0; width: 0; margin: 0; padding: 0; text-decoration: none; border: none;'
                    ],
                    'row_attr' => [
                        'style' => 'margin: 0 !important; height: 0; width: 0; color:transparent;'
                    ],
                    'constraints' => new Assert\Callback([$this, 'manageSpam']),
                    'required' => false,
                ]);
    }

    public function manageSpam($data, ExecutionContextInterface $context): void
    {
        $form = $context->getRoot();

        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();

        $fakeEmail = ($form->get("honeypot"))->get('fakeEmail')->getData();
        $ip = $request->getClientIp();

        if ($fakeEmail) {

            if ($this->logger) {
                $this->logger->alert("Spam tried with $fakeEmail from $ip");
            }

            $context
                ->buildViolation("Spam tried with $fakeEmail from $ip")
                ->setCode(self::CODE)
                ->setParameters(['email' => $fakeEmail, 'ip' => $ip])
                ->addViolation();

            $this->dispatchHoneypotSpamDetectedEvent($fakeEmail, $ip);
        }
    }

    private function dispatchHoneypotSpamDetectedEvent($fakeEmail, ?string $ip): void
    {
        $event = new HoneypotSpamDetectedEvent($fakeEmail, $ip);
        $this->eventDispatcher->dispatch($event, HoneypotSpamDetectedEvent::NAME);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'mapped' => false,
            'required' => false,
        ]);
    }
}

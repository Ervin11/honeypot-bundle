<?php

namespace Ervin11\HoneypotBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class HoneypotSpamDetectedEvent extends Event {

    public const NAME = 'honeypot.spam.detected';

    protected $email;

    protected $ip;

    public function __construct(string $email, string $ip)
    {
        $this->email = $email;
        $this->ip = $ip;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getIp(): string
    {
        return $this->ip;
    }
}

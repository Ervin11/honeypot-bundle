<?php

namespace Ervin11\HoneypotBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait HoneypotableTrait {

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected ?string $email;

    protected string $fakeEmail;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getFakeEmail(): ?string
    {
        return $this->fakeEmail;
    }

    public function setFakeEmail(string $fakeEmail): self
    {
        $this->fakeEmail = $fakeEmail;
        return $this;
    }
}

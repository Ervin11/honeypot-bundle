<?php

namespace Ervin11\HoneypotBundle;

use Honeypot\HoneypotBundle\DependencyInjection\HoneypotExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class HoneypotBundle extends Bundle {}
